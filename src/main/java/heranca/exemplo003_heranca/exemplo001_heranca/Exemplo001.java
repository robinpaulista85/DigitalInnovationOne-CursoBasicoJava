package heranca.exemplo003_heranca.exemplo001_heranca;


public class Exemplo001 {

    public static void main(String[] args) {

        Carro carro = new Carro();
        carro.setMarca("Nissan");
        carro.setModelo("March");
        carro.setQuantidadeDePortas(4);

        Motocicleta moto = new Motocicleta();
        moto.setMarca("Ducati");
        moto.setModelo("Street Fighter");
        moto.setCilindradas("850");

    }


}
