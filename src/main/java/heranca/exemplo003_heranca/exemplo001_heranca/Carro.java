package heranca.exemplo003_heranca.exemplo001_heranca;

public class Carro extends Veiculo{

    private int quantidadeDePortas;

    public int getQuantidadeDePortas() { return quantidadeDePortas; }

    public void setQuantidadeDePortas(int quantidadeDePortas) {
        this.quantidadeDePortas = quantidadeDePortas;
    }
}
