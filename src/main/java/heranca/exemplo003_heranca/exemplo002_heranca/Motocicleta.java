package heranca.exemplo003_heranca.exemplo002_heranca;

public class Motocicleta extends Veiculo{

    private String cilindradas;

    public String getCilindradas() {
        return cilindradas;
    }

    public void setCilindradas(String cilindradas) {
        this.cilindradas = cilindradas;
    }
}
